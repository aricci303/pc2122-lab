// Agent sample_agent in project my_first_jacamo_program

/* Initial beliefs and rules */

language("ita").


/* Initial goals */

!greet(5).

/* Plans */


+!greet(N) : true 
	<- +greet_count(0);
	   !greet_until(N).

+!greet_until(N) : greet_count(M) & (M < N)
	<- .print("hello world");
	   -+greet_count(M + 1);
	   !greet_until(N).
	   
+!greet_until(N) : greet_count(M) & (M >= N)
	<- .println("done").
	 	   

	   
	   

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

// uncomment the include below to have an agent compliant with its organisation
//{ include("$moiseJar/asl/org-obedient.asl") }
