package pc.wot00_basic;

import io.vertx.core.Vertx;

public class RunGateThingService {

	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();
		vertx.deployVerticle(new GateThingService(8880));
	}

}
