package pc.wot00_basic;

import java.util.Iterator;
import java.util.LinkedList;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class GateThingService extends AbstractVerticle {

	private HttpServer server;
	private Router router;

	private int thingPort;

	// private static final int DONE = 201;
	private static final int ACCEPTED = 202;
	private static final int REJECTED = 403;
	
	private static final String PROPERTY_STATUS = "/api/properties/status";
	private static final String ACTION_OPEN = "/api/actions/open";
	private static final String ACTION_CLOSE = "/api/actions/close";
	private static final String EVENTS = "/api/events";

	private static final int OPENING_DURATION_IN_MS = 1000;
	private static final int CLOSING_DURATION_IN_MS = 1000;
	
	// lamp state
	private enum Status { OPEN, CLOSE, OPENING, CLOSING} 
	private Status status;
	private long ongoingActionId;
	
	// event support
	private LinkedList<ServerWebSocket> subscribers;
	

	public GateThingService(int port) {
		this.thingPort = port;
		status = Status.CLOSE;
	}
	
	protected void setupAPI() {
		router = Router.router(vertx);		
		try {
			router.get(PROPERTY_STATUS).handler(this::handleGetPropertyStatus);			
			router.post(ACTION_OPEN).handler(this::handleActionOpen);	
			router.post(ACTION_CLOSE).handler(this::handleActionClose);			
		} catch (Exception ex) {
			log("API setup failed - " + ex.toString());
		}
		subscribers = new LinkedList<ServerWebSocket>();
	
	}

	protected void handleGetPropertyStatus(RoutingContext ctx) {
		HttpServerResponse res = ctx.response();
		res.putHeader("Content-Type", "application/json");
		JsonObject reply = new JsonObject();
		reply.put("status", getStatusLabel(status));
		res.end(reply.toBuffer());
	}
	
	protected void handleActionOpen(RoutingContext ctx) {
		HttpServerResponse res = ctx.response();
		if (status.equals(Status.CLOSE) || status.equals(Status.CLOSING) ) {
			if (status.equals(Status.CLOSING)) {
				vertx.cancelTimer(ongoingActionId);
			}
			status = Status.OPENING;
			log("Gate opening");
			JsonObject ev = new JsonObject();
			ev.put("time", System.currentTimeMillis());
			ev.put("event", "opening");
			this.notifyEvent(ev);					
			ongoingActionId = vertx.setTimer(OPENING_DURATION_IN_MS, p -> {
				status = Status.OPEN;
				log("Gate opened");
				JsonObject evOp = new JsonObject();
				evOp.put("time", System.currentTimeMillis());
				evOp.put("event", "opened");
				this.notifyEvent(evOp);		
			});
			res.setStatusCode(ACCEPTED).end();
		} else {
			res.setStatusCode(REJECTED).end();
		}	
	}
	
	protected void handleActionClose(RoutingContext ctx) {
		HttpServerResponse res = ctx.response();
		if (status.equals(Status.OPEN) || status.equals(Status.OPENING) ) {
			if (status.equals(Status.OPENING)) {
				vertx.cancelTimer(ongoingActionId);
			}
			status = Status.CLOSING;
			log("Gate closing");
			JsonObject ev = new JsonObject();
			ev.put("time", System.currentTimeMillis());
			ev.put("event", "closing");
			this.notifyEvent(ev);					
			ongoingActionId = vertx.setTimer(CLOSING_DURATION_IN_MS, p -> {
				status = Status.CLOSE;
				log("Gate closed");
				JsonObject evOp = new JsonObject();
				evOp.put("time", System.currentTimeMillis());
				evOp.put("event", "closed");
				this.notifyEvent(evOp);		
			});
			res.setStatusCode(ACCEPTED).end();
		} else {
			res.setStatusCode(REJECTED).end();
		}	
	}
	
	protected void spawnServer(int port, Promise<Void> startPromise) {		
		server = vertx.createHttpServer();
		server
		.webSocketHandler(ws -> {
			if (!ws.path().equals(EVENTS)) {
				ws.reject();
			} else {
				log("New subscriber from " + ws.remoteAddress());
				subscribers.add(ws);
			}
		})
		.requestHandler(router)
		.listen(port, http -> {
			if (http.succeeded()) {
				startPromise.complete();
				log("GateThing server started on port " + port);
			} else {
				log("GateThing server failure " + http.cause());
				startPromise.fail(http.cause());
			}
		});
	}

	protected String getStatusLabel(Status s) {
		switch (s) {
		case OPEN : return "open";
		case CLOSE : return "close";
		case OPENING : return "opening";
		case CLOSING : return "closing";
		}
		return null;
	}
	
	protected void notifyEvent(JsonObject ev) {
		Iterator<ServerWebSocket> it = this.subscribers.iterator();
		while (it.hasNext()) {
			ServerWebSocket ws = it.next();
			if (!ws.isClosed()) {
				try {
					ws.write(ev.toBuffer());
				} catch (Exception ex) {
					it.remove();
				}
			} else {
				it.remove();
			}
		}
	}
	
	@Override
	public void start(Promise<Void> startPromise) throws Exception {
		setupAPI();
		spawnServer(this.thingPort, startPromise);
	}

	
	protected void log(String msg) {
		System.out.println("[GateThingService]["+System.currentTimeMillis()+"] " + msg);
	}
	

}
