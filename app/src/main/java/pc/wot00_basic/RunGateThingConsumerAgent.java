package pc.wot00_basic;

import io.vertx.core.Vertx;

/**
 * Running the lamp consumer agent.
 * 
 * @author aricci
 *
 */
public class RunGateThingConsumerAgent {

	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();
		vertx.deployVerticle(new GateThingConsumerAgent("localhost", 8880));
	}

}
