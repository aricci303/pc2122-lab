package pc.wot00_basic;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.WebSocket;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;

public class GateThingConsumerAgent extends AbstractVerticle {

	private static final String PROPERTY_STATUS = "/api/properties/status";
	private static final String ACTION_OPEN = "/api/actions/open";
	private static final String ACTION_CLOSE = "/api/actions/close";
	private static final String EVENTS = "/api/events";

	private WebClient client;
	private int thingPort;
	private String thingHost;
	
	
	public GateThingConsumerAgent(String thingHost, int thingPort) {
		this.thingPort = thingPort;
		this.thingHost = thingHost;
	}
	
	@Override
	public void start(Promise<Void> startPromise) throws Exception {

		log("Gate consumer started.");		
		setupClient();
		
		log("Subscribing...");
		Future<Void> subRes = subscribe();
		Future<JsonObject> getStatusRes = subRes.compose(res3 -> {
			log("Subscribed!");
			return getStatus();
		}).onFailure(err -> {
			log("error " + err);
		});
		
		log("Open the gate...");
		Future<Void> doOpenRes = getStatusRes.compose(res -> {
			log("Status received: \n" + res.encodePrettily());	
			return this.doOpen();
		});
		
		doOpenRes.onComplete(res2 -> {
			log("Action accepted. "); 
		});
				
	}
	
	protected void setupClient() {
		client = WebClient.create(vertx);
	}
	
	protected Future<JsonObject> getStatus() {
		Promise<JsonObject> promise = Promise.promise();
		client
			.get(this.thingPort, thingHost, PROPERTY_STATUS)
			.send()
			.onSuccess(response -> {
				JsonObject reply = response.bodyAsJsonObject();
				promise.complete(reply);
			})
			.onFailure(err -> {
				promise.fail("Something went wrong " + err.getMessage());
			});
		return promise.future();
	}
	
	protected Future<Void> doOpen() {
		Promise<Void> promise = Promise.promise();
		client
			.post(this.thingPort, thingHost, ACTION_OPEN)
			.send()
			.onSuccess(response -> {
				promise.complete(null);
			})
			.onFailure(err -> {
				promise.fail("Something went wrong " + err.getMessage());
			});
		return promise.future();
	}

	protected Future<Void> doClose() {
		Promise<Void> promise = Promise.promise();
		client
			.post(this.thingPort, thingHost, ACTION_CLOSE)
			.send()
			.onSuccess(response -> {
				promise.complete(null);
			})
			.onFailure(err -> {
				promise.fail("Something went wrong " + err.getMessage());
			});
		return promise.future();
	}
	
	protected Future<Void> subscribe() {
		Promise<Void> promise = Promise.promise();
		HttpClient cli = vertx.createHttpClient();
		cli.webSocket(this.thingPort, thingHost, EVENTS, res -> {
			if (res.succeeded()) {
				promise.complete();
				log("Connected!");
				WebSocket ws = res.result();
				ws.handler(this::onNewEvent);
			}
		});
		return promise.future();
			
	}

	protected void onNewEvent(Buffer buf) {
		log("New event: " + buf.toString());
	}
	
	protected void log(String msg) {
		System.out.println("[GateThingConsumerAgent]["+System.currentTimeMillis()+"] " + msg);
	}
	
	
}
