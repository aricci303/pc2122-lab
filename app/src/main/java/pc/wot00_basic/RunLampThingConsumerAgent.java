package pc.wot00_basic;

import io.vertx.core.Vertx;

public class RunLampThingConsumerAgent {

	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();
		vertx.deployVerticle(new LampThingConsumerAgent("localhost", 8888));
	}

}
