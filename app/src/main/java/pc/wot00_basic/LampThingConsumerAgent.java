package pc.wot00_basic;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.WebSocket;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;

/**
 * Toy example of an event-loop based agent interacting with a Lamp thing.
 * 
 * @author aricci
 *
 */
public class LampThingConsumerAgent extends AbstractVerticle {

	private WebClient client;

	private int thingPort;
	private String thingHost;
	private static final String PROPERTY_STATUS = "/api/properties/status";
	private static final String ACTION_SWITCHON = "/api/actions/switchOn";
	private static final String EVENTS = "/api/events";
	
	private int nEventsReceived;
	
	public LampThingConsumerAgent(String thingHost, int thingPort) {
		this.thingPort = thingPort;
		this.thingHost = thingHost;
		nEventsReceived = 0;
	}
	
	/**
	 * Main agent body.
	 */
	public void start(Promise<Void> startPromise) throws Exception {
		log("Lamp consumer agent started.");		
		setupClient();
		
		log("Getting the status...");		
		Future<JsonObject> getStatusActionRes = getStatus();

		Future<Void> doSwitchOnRes = getStatusActionRes.compose(res -> {
			log("Status received: \n" + res.encodePrettily());			
			log("Switching on");
			return this.doSwitchOn();
		}).onFailure(err -> {
			log("Failure " + err);
		});
		
		Future<Void> subscribeRes = doSwitchOnRes.compose(res2 -> {
			log("Action done. "); 				
			log("Subscribing...");
			return subscribe();
		});

		subscribeRes.onComplete(res3 -> {
			log("Subscribed!");
		});
		
		/* the same code, in CPS style */
		/* 
		log("Getting the status...");		
		Future<JsonObject> fut = getStatus();
		fut.onComplete(res -> {
			log("Status received: \n" + res.result().encodePrettily());			
			log("Switching on");

			Future<Void> action = this.doSwitchOn();
			action.onComplete(res2 -> {
				log("Action done. "); 				
				log("Subscribing...");
				Future<Void> sub = subscribe();
				sub.onComplete(res3 -> {
					log("Subscribed!");
				});
			});
			
		}).onFailure(err -> {
			log("Failure " + err);
		});
		 */
	
	}
	
	/* ACTIONS */
	
	protected void setupClient() {
		client = WebClient.create(vertx);
	}
	
	/**
	 * Action to get the status of the lamp
	 * @return
	 */
	protected Future<JsonObject> getStatus() {
		Promise<JsonObject> promise = Promise.promise();
		client
			.get(this.thingPort, thingHost, PROPERTY_STATUS)
			.send()
			.onSuccess(response -> {
				JsonObject reply = response.bodyAsJsonObject();
				promise.complete(reply);
			})
			.onFailure(err -> {
				promise.fail("Something went wrong " + err.getMessage());
			});
		return promise.future();
	}
	
	/**
	 * Action to switch on the lamp
	 * @return
	 */
	protected Future<Void> doSwitchOn() {
		Promise<Void> promise = Promise.promise();
		client
			.post(this.thingPort, thingHost, ACTION_SWITCHON)
			.send()
			.onSuccess(response -> {
				promise.complete(null);
			})
			.onFailure(err -> {
				promise.fail("Something went wrong " + err.getMessage());
			});
		return promise.future();
	}

	/**
	 * Action to subscribe
	 * @return
	 */
	protected Future<Void> subscribe() {
		Promise<Void> promise = Promise.promise();
		HttpClient cli = vertx.createHttpClient();
		cli.webSocket(this.thingPort, thingHost, EVENTS, res -> {
			if (res.succeeded()) {
				log("Connected!");
				WebSocket ws = res.result();
				ws.handler(this::onNewEvent);
				promise.complete();
			}
		});
		return promise.future();			
	}
	
	/**
	 * Handler to process observed events  
	 */
	protected void onNewEvent(Buffer buf) {
		nEventsReceived++;
		log("New event: " + buf.toString() + "( " + nEventsReceived + " events received)");
	}
	
	protected void log(String msg) {
		System.out.println("[LampThingConsumerAgent]["+System.currentTimeMillis()+"] " + msg);
	}
	
}
