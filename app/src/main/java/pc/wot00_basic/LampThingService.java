package pc.wot00_basic;

import java.util.Iterator;
import java.util.LinkedList;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

/**
 * Toy Lamp Thing service example based on an event-loop reactive architecture
 * 
 * @author aricci
 *
 */
public class LampThingService extends AbstractVerticle {

	/* API REST routes */
	private static final String PROPERTY_STATUS = "/api/properties/status";
	private static final String ACTION_SWITCHON = "/api/actions/switchOn";
	private static final String ACTION_SWITCHOFF = "/api/actions/switchOff";
	private static final String EVENTS = "/api/events";


	/* Thing state */
	
	private boolean isOn;

	/* http implementation support */
	
	private HttpServer server;
	private Router router;
    private int thingPort;
	private static final int DONE = 201;

	// Subscribers
	private LinkedList<ServerWebSocket> subscribers;
	
	
	public LampThingService(int port) {
		this.thingPort = port;
		isOn = false;		
	}
	
	/**
	 * Setup REST API routes
	 * 
	 */
	protected void setupAPI() {
		router = Router.router(vertx);		
		try {
			router.get(PROPERTY_STATUS).handler(this::handleGetPropertyStatus);			
			router.post(ACTION_SWITCHON).handler(this::handleActionSwitchOn);	
			router.post(ACTION_SWITCHOFF).handler(this::handleActionSwitchOff);			
		} catch (Exception ex) {
			log("API setup failed - " + ex.toString());
		}
		subscribers = new LinkedList<ServerWebSocket>();
	
	}

	/**
	 * Handler for status property
	 * 
	 * @param ctx
	 */
	protected void handleGetPropertyStatus(RoutingContext ctx) {
		HttpServerResponse res = ctx.response();
		res.putHeader("Content-Type", "application/json");
		JsonObject reply = new JsonObject();
		reply.put("status", isOn ? "on" : "off");
		res.end(reply.toBuffer());
	}
	
	/**
	 * 
	 * Handler for switchOn action
	 * 
	 * @param ctx
	 */
	protected void handleActionSwitchOn(RoutingContext ctx) {
		HttpServerResponse res = ctx.response();
		isOn = true;
		log("Lamp switched on");
		JsonObject ev = new JsonObject();
		ev.put("time", System.currentTimeMillis());
		ev.put("event", "switchedOn");
		this.notifyEvent(ev);		
		res.setStatusCode(DONE).end();
	}
	
	/**
	 * 
	 * Handler for switchOff action
	 * 
	 * @param ctx
	 */
	protected void handleActionSwitchOff(RoutingContext ctx) {
		HttpServerResponse res = ctx.response();
		isOn = false;
		log("Lamp switched off");
		JsonObject ev = new JsonObject();
		ev.put("time", System.currentTimeMillis());
		ev.put("event", "switchedOff");
		this.notifyEvent(ev);		
		res.setStatusCode(DONE).end();
	}
	
	/**
	 * 
	 * Spawn the HTTP server
	 * 
	 * @param ctx
	 */
	protected void spawnServer(int port, Promise<Void> startPromise) {		
		server = vertx.createHttpServer();
		server
		.webSocketHandler(ws -> {
			if (!ws.path().equals(EVENTS)) {
				ws.reject();
			} else {
				log("New subscriber from: " + ws.remoteAddress());
				subscribers.add(ws);
			}
		})
		.requestHandler(router)
		.listen(port, http -> {
			if (http.succeeded()) {
				startPromise.complete();
				log("LampThing server started on port " + port);
			} else {
				log("LampThing server failure " + http.cause());
				startPromise.fail(http.cause());
			}
		});
	}

	/* 
	 * To notify events to subscribers
	 * 
	 */
	protected void notifyEvent(JsonObject ev) {
		Iterator<ServerWebSocket> it = this.subscribers.iterator();
		while (it.hasNext()) {
			ServerWebSocket ws = it.next();
			if (!ws.isClosed()) {
				try {
					ws.write(ev.toBuffer());
				} catch (Exception ex) {
					it.remove();
				}
			} else {
				log("subscription closed.");
				it.remove();
			}
		}
	}
	
	@Override
	public void start(Promise<Void> startPromise) throws Exception {
		setupAPI();
		spawnServer(this.thingPort, startPromise);
	}
	
	protected void log(String msg) {
		System.out.println("[LampThingService]["+System.currentTimeMillis()+"] " + msg);
	}
	
}
