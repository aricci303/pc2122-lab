package pc.wot01_refactorised;

import io.vertx.core.Promise;
import io.vertx.core.Vertx;

public abstract class AbstractAdapter<Model> {

	private Vertx vertx;
	
	// lamp model
	private Model model;
	
	protected AbstractAdapter(Model model, Vertx vertx) {
		this.model = model;
		this.vertx = vertx;
	}
	
	protected Vertx getVertx() {
		return vertx;
	}

	protected Model getModel() {
		return model;
	}
	
	abstract protected void setupAdapter(Promise<Void> startPromise);
	
}
