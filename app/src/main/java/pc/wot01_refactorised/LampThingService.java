package pc.wot01_refactorised;

import java.util.logging.Level;
import java.util.logging.Logger;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;

public class LampThingService extends AbstractVerticle {

	private LampThingModel model;
	private LampThingHTTPAdapter httpAPI;
	
	private Logger logger = Logger.getLogger(LampThingService.class.getName());
	private int port;
	

	public LampThingService(int port) {
		model = new LampThingModel();
		this.port = port;
	}
		
	@Override
	public void start(Promise<Void> startPromise) throws Exception {
		logger.setLevel(Level.ALL);
		httpAPI = new LampThingHTTPAdapter(model, port, this.getVertx());
		httpAPI.setupAdapter(startPromise);
	}
}
