package pc.wot01_refactorised;

public class LampThingModel {

	private boolean isOn;
	
	public LampThingModel() {
		isOn = false;
		log("LampThing model created.");
	}
	
	public void switchOn() {
		isOn = true;
		log("Switched on.");
	}

	public void switchOff() {
		isOn = false;
		log("Switched off.");
	}

	public boolean isOn() {
		return isOn;
	}
	
	protected void log(String msg) {
		System.out.println("[LampThingModel]["+System.currentTimeMillis()+"] " + msg);
	}
	
}
