package pc.wot01_refactorised;

import io.vertx.core.Vertx;

public class RunLampThingService {

	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();
		vertx.deployVerticle(new LampThingService(8888));
	}

}
