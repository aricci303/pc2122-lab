package pc.wot01_refactorised;

import java.util.Iterator;
import java.util.LinkedList;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class LampThingHTTPAdapter extends AbstractAdapter<LampThingModel>{

	private HttpServer server;
	private Router router;

	private int thingPort;
	
	private static final int DONE = 201;
	
	private static final String PROPERTY_STATUS = "/api/properties/status";
	private static final String ACTION_SWITCHON = "/api/actions/switchOn";
	private static final String ACTION_SWITCHOFF = "/api/actions/switchOff";
	private static final String EVENTS = "/api/events";
	
	// event support
	private LinkedList<ServerWebSocket> subscribers;
	
	public LampThingHTTPAdapter(LampThingModel model, int port, Vertx vertx) {
		super(model, vertx);
		this.thingPort = port;
	}
	
	protected void setupAdapter(Promise<Void> startPromise) {
		router = Router.router(this.getVertx());		
		try {
			router.get(PROPERTY_STATUS).handler(this::handleGetPropertyStatus);			
			router.post(ACTION_SWITCHON).handler(this::handleActionSwitchOn);	
			router.post(ACTION_SWITCHOFF).handler(this::handleActionSwitchOff);			
		} catch (Exception ex) {
			log("API setup failed - " + ex.toString());
		}
		subscribers = new LinkedList<ServerWebSocket>();
	
		server = this.getVertx().createHttpServer();
		server
		.webSocketHandler(ws -> {
			if (!ws.path().equals(EVENTS)) {
				ws.reject();
			} else {
				log("New subscriber from " + ws.remoteAddress());
				subscribers.add(ws);
			}
		})
		.requestHandler(router)
		.listen(thingPort, http -> {
			if (http.succeeded()) {
				startPromise.complete();
				log("HTTP Thing Adapter started on port " + thingPort);
			} else {
				log("HTTP Thing Adapter failure " + http.cause());
				startPromise.fail(http.cause());
			}
		});
	}

	protected void handleGetPropertyStatus(RoutingContext ctx) {
		HttpServerResponse res = ctx.response();
		res.putHeader("Content-Type", "application/json");
		JsonObject reply = new JsonObject();
		reply.put("status", this.getModel().isOn() ? "on" : "off");
		res.end(reply.toBuffer());
	}
	
	protected void handleActionSwitchOn(RoutingContext ctx) {
		HttpServerResponse res = ctx.response();
		log("Switch on request.");
		this.getModel().switchOn();
		JsonObject ev = new JsonObject();
		ev.put("time", System.currentTimeMillis());
		ev.put("event", "switchedOn");
		this.notifyEvent(ev);		
		res.setStatusCode(DONE).end();
	}
	
	protected void handleActionSwitchOff(RoutingContext ctx) {
		HttpServerResponse res = ctx.response();
		log("Switch off request.");
		this.getModel().switchOff();
		JsonObject ev = new JsonObject();
		ev.put("time", System.currentTimeMillis());
		ev.put("event", "switchedOff");
		this.notifyEvent(ev);		
		res.setStatusCode(DONE).end();
	}
	
	protected void notifyEvent(JsonObject ev) {
		Iterator<ServerWebSocket> it = this.subscribers.iterator();
		while (it.hasNext()) {
			ServerWebSocket ws = it.next();
			if (!ws.isClosed()) {
				try {
					ws.write(ev.toBuffer());
				} catch (Exception ex) {
					it.remove();
				}
			} else {
				it.remove();
			}
		}
	}
	
	protected void log(String msg) {
		System.out.println("[LampThingHTTPAdapter]["+System.currentTimeMillis()+"] " + msg);
	}
	
}
