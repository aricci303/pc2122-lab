package pc.mix;

import java.util.List;

import com.apicatalog.jsonld.*;
import com.apicatalog.rdf.RdfDataset;
import com.apicatalog.rdf.RdfGraph;
import com.apicatalog.rdf.RdfTriple;

public class TestTitanium {

	public static void main(String[] args) throws Exception {

		// System.out.println(JsonLd.toRdf("https://w3c.github.io/json-ld-api/tests/expand/0001-in.jsonld").get());
		RdfDataset ds = JsonLd.toRdf("http://localhost:8080/counter").get();

		RdfGraph graph = ds.getDefaultGraph();		
		List<RdfTriple> list = graph.toList();
		for (RdfTriple t: list) {
			System.out.println(t);
		}
		
	}

}
