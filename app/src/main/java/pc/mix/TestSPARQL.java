package pc.mix;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdfconnection.RDFConnection;
import org.apache.jena.rdfconnection.RDFConnectionFactory;

public class TestSPARQL {

	public static void main(String ...args) {
	    Query query = QueryFactory.create("SELECT * { <http://example.org/book/book1> ?p ?o }");
	    String queryService = "http://sparql.org/books/query";

	    // Query service, no update, no graph store protocol.
	    try ( RDFConnection conn = RDFConnectionFactory.connect(queryService, null, null) ) {
	      conn.queryResultSet(query, ResultSetFormatter::out);
	    }
	}
}

