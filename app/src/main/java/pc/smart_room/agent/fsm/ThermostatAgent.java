package pc.smart_room.agent.fsm;

import java.util.HashMap;
import java.util.concurrent.Callable;
import java.util.function.Supplier;

import pc.smart_room.api.RoomThingAPI;

/**
 * 
 * Thread based agent whose task is to achieve and maintain a target temperature,
 * which can be specified and changed dynamically by a user through a GUI.
 * 
 * @author aricci
 *
 */
public class ThermostatAgent extends AbstractThreadBasedThermostatAgent {

	private static double THRESOLD = 0.5;
	private static String IDLE = "idle";
	private static String COOLING = "cooling";
	private static String HEATING = "heating";
	private double targetGoalTemperature;
	private static long AGENT_CYCLE_PERIOD = 500;
	private 	UserPrefPanel panel;

	
	private enum State { INIT, WAIT_USER_START, IDLE, WORKING_TOO_HOT, WORKING_TOO_COLD }
	private State currentState;
	private HashMap<State,Callable<State>> behaviours;
	
	public ThermostatAgent(RoomThingAPI roomThing) {
		super(roomThing);
		currentState = State.INIT;
		behaviours = new HashMap<State,Callable<State>>();
		
		behaviours.put(State.INIT,this::initBehaviour);
		behaviours.put(State.WAIT_USER_START,this::waitUserStartingBehaviour);
		behaviours.put(State.IDLE,this::idleBehaviour);
		behaviours.put(State.WORKING_TOO_COLD,this::tooColdBehaviour);
		behaviours.put(State.WORKING_TOO_HOT,this::tooHotBehaviour);
	}
	
	/* main execution cycle of the agent */
	
	public void run() {
		while (true) {
			try {
				this.sleep(AGENT_CYCLE_PERIOD);
				currentState = behaviours.get(currentState).call();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	
	/* behaviours */
	
	private State initBehaviour() {
		log("launched - working with room thing " + this.getThingId());
		log("setting up GUI.");
		createUserGUI();
		return State.WAIT_USER_START;
	}

	private State waitUserStartingBehaviour() {
		log("waiting for user start.");
		waitForUserStart();		
		return State.IDLE;
	}

	private State idleBehaviour() {
		double currentTemp = this.doGetTemperature();
		targetGoalTemperature = this.getUserTargetTemperature();
				
		log("Idle (State: current temperature: " + currentTemp + " - state: " + currentState +" - target temp: " + targetGoalTemperature + ")" );
		double dt = currentTemp - targetGoalTemperature;
		try {
			if (Math.abs(dt) > THRESOLD) {
				if (dt > THRESOLD) {
					this.doStartCooling();
					return State.WORKING_TOO_HOT;
				} else {
					this.doStartHeating();
					return State.WORKING_TOO_COLD;
				}
			} else {
				return State.IDLE;
			}
		} catch (Exception ex) {
			return State.IDLE;
		}
	}

	private State tooHotBehaviour() {
		double currentTemp = this.doGetTemperature();
		String roomState = this.doGetState();
		targetGoalTemperature = this.getUserTargetTemperature();

		log("Acting (State: current temperature: " + currentTemp + " - state: " + roomState +" - target temp: " + targetGoalTemperature + ")" );
		try {
			if (currentTemp < (targetGoalTemperature - THRESOLD) && !roomState.equals(HEATING)) {
				log("too cold: start heating...");
				this.doStartHeating();
				return State.WORKING_TOO_COLD;
			} else if (Math.abs(currentTemp - targetGoalTemperature) < THRESOLD) {
				log("achieved: stop working");
				if (!roomState.equals(IDLE)) {
					this.doStopWorking();
				}
				return State.IDLE;
			} else {
				return State.WORKING_TOO_HOT;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return State.IDLE;
		}
	}

	private State tooColdBehaviour() {
		double currentTemp = this.doGetTemperature();
		String roomState = this.doGetState();
		targetGoalTemperature = this.getUserTargetTemperature();

		try {
			log("Acting (State: current temperature: " + currentTemp + " - state: " + roomState +" - target temp: " + targetGoalTemperature + ")" );
			if (currentTemp > (targetGoalTemperature + THRESOLD) && !roomState.equals(COOLING)) {
				log("too hot: start cooling...");
				this.doStartCooling();
				return State.WORKING_TOO_HOT;
			} else if (Math.abs(currentTemp - targetGoalTemperature) < THRESOLD) {
				log("achieved: stop working");
				if (!roomState.equals(IDLE)) {
					this.doStopWorking();
				}
				return State.IDLE;
			} else {
				return State.WORKING_TOO_COLD;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return State.IDLE;
			
		}
	}
	
	/* actions */
	
	protected void createUserGUI() {
		panel = new 	UserPrefPanel();
		panel.display();
	}
	
	protected double getUserTargetTemperature() {
		return panel.getCurrentUserTemperature();
	}
	
	protected void waitForUserStart() {
		try {
			panel.waitForStart();
		} catch (Exception ex) {}
	}
}
