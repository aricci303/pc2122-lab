package pc.smart_room.agent.fsm;

import pc.smart_room.api.RoomThingProxy;

public class ThermostatAgentLauncher {

	public static void main(String[] args) {
		RoomThingProxy thing = new RoomThingProxy("my-room", "localhost", 8888);
		new ThermostatAgent(thing).start();
	}	

}
