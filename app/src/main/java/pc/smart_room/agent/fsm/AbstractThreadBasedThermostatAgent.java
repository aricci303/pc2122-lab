package pc.smart_room.agent.fsm;

import java.util.concurrent.Semaphore;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import pc.smart_room.api.RoomThingAPI;

/**
 * Abstract base class for thread-based thermostat agent
 * @author aricci
 *
 */
public abstract class AbstractThreadBasedThermostatAgent extends Thread {
	
	private RoomThingAPI roomThing;
	
	public AbstractThreadBasedThermostatAgent(RoomThingAPI roomThing) {
		this.roomThing = roomThing;
	}
		
	// actions
	
	protected String getThingId() {
		return roomThing.getId();
	}
	
	protected String doGetState() {
		final Semaphore ready = new Semaphore(0);
		Holder<String> result = new Holder();
		
		Future<String> fut = roomThing.getState();
		fut.onSuccess(res -> {
			  result.set(res);
			  ready.release();
		}).onFailure(f -> {
		      System.out.println("Something went wrong " + f.getMessage());
			  ready.release();
		});
		
		try {
			ready.acquire();
			return result.get();
		} catch (Exception ex) {
			return null;
		}
	}

	protected Double doGetTemperature() {
		final Semaphore ready = new Semaphore(0);
		Holder<Double> result = new Holder();
		
		Future<Double> fut = roomThing.getTemperature();
		fut.onSuccess(res -> {
			  result.set(res);
			  ready.release();
		}).onFailure(f -> {
		      System.out.println("Something went wrong " + f.getMessage());
			  ready.release();
		});
		
		try {
			ready.acquire();
			return result.get();
		} catch (Exception ex) {
			return null;
		}
	}

	
	protected void doStartCooling() throws Exception {
		final Semaphore ready = new Semaphore(0);
		Holder<Boolean> result = new Holder();
		Future<Void> fut = roomThing.startCooling();
		fut.onSuccess(res -> {
			result.set(true);
			ready.release();			
		}).onFailure(f -> {
			result.set(false);
			ready.release();
		});
		
		try {
			ready.acquire();
		} catch (Exception ex) {
		}
		if (!result.get()) {
			throw new Exception();
		}

	}

	protected void doStartHeating() throws Exception {
		final Semaphore ready = new Semaphore(0);
		Holder<Boolean> result = new Holder();
		Future<Void> fut = roomThing.startHeating();
		fut.onSuccess(res -> {
			result.set(true);
			ready.release();			
		}).onFailure(f -> {
			result.set(false);
			ready.release();
		});
		
		try {
			ready.acquire();
		} catch (Exception ex) {
		}
		if (!result.get()) {
			throw new Exception();
		}
	}
	
	protected void doStopWorking() throws Exception {
		final Semaphore ready = new Semaphore(0);
		Holder<Boolean> result = new Holder();
		Future<Void> fut = roomThing.stopWorking();
		fut.onSuccess(res -> {
			result.set(true);
			ready.release();			
		}).onFailure(f -> {
			result.set(false);
			ready.release();
		});
		
		try {
			ready.acquire();
		} catch (Exception ex) {
		}
		if (!result.get()) {
			throw new Exception();
		}

	}

	protected void log(String msg) {
		System.out.println("[THERMOSTAT] " + msg);
	}
	
	protected void waitTimeInMs(long dt) {
		try {
			Thread.sleep(dt);
		} catch (Exception ex) {};
	}	


}
