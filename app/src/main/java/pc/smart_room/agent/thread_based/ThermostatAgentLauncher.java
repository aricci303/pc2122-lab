package pc.smart_room.agent.thread_based;

import pc.smart_room.api.RoomThingProxy;

public class ThermostatAgentLauncher {
	
	public static void main(String[] args) {
		
		RoomThingProxy thing = new RoomThingProxy("my-room", "localhost", 8888);
		
		new ThermostatAgentStep1(thing, 17).start();
		// new ThermostatAgentStep2(thing, 17).start();
		// new ThermostatAgentStep3(thing).start();

	}	
}
