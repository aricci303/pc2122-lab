package pc.smart_room.agent.thread_based;

import pc.smart_room.api.RoomThingAPI;

/**
 * 
 * Thread based agent whose task is to achieve and maintain a target temperature.
 * 
 * @author aricci
 *
 */

public class ThermostatAgentStep2 extends AbstractThreadBasedThermostatAgent {

	private static double THRESOLD = 0.5;
	private static String IDLE = "idle";
	private static String COOLING = "cooling";
	private static String HEATING = "heating";
	private double targetGoalTemperature;
	private static long AGENT_CYCLE_PERIOD = 500;
	private static long AGENT_CYCLE_LONG_PERIOD = 1000;
	

	public ThermostatAgentStep2(RoomThingAPI roomThing, double targetGoalTemperature) {
		super(roomThing);
		this.targetGoalTemperature = targetGoalTemperature;
	}
	
	public void run() {
		log("launched - working with room thing " + this.getThingId() + " - temperature to achieve and maintain: " + targetGoalTemperature);		
		while (true) {
			double currentTemp = this.doGetTemperature();
			String state = this.doGetState();
			log("Idle - State: current temperature: " + currentTemp + " - state: " + state );
			if (Math.abs(currentTemp - targetGoalTemperature) > THRESOLD) {
				boolean tempAchieved = false;
				while (!tempAchieved) {
					try {
						currentTemp = this.doGetTemperature();
						state = this.doGetState();
						log("Acting (State: current temperature: " + currentTemp + " - state: " + state +")" );
						if (currentTemp < (targetGoalTemperature - THRESOLD) && !state.equals(HEATING)) {
							log("too cold: start heating...");
							this.doStartHeating();
						} else if (currentTemp > (targetGoalTemperature + THRESOLD) && !state.equals(COOLING)) {
							log("too hot: start cooling...");
							this.doStartCooling();
						} else if (Math.abs(currentTemp - targetGoalTemperature) < THRESOLD) {
							log("achieved: stop working");
							if (!state.equals(IDLE)) {
								this.doStopWorking();
							}
							tempAchieved = true;
						}
						waitTimeInMs(AGENT_CYCLE_PERIOD);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
			waitTimeInMs(AGENT_CYCLE_LONG_PERIOD);
		}
	}
}
