package pc.smart_room.agent.thread_based;

import pc.smart_room.api.RoomThingAPI;

/**
 * 
 * Thread based agent whose task is to achieve a target temperature,
 * eventually reacting to temperature changes while achieving it.
 * 
 * @author aricci
 *
 */
public class ThermostatAgentStep1 extends AbstractThreadBasedThermostatAgent {

	private static double THRESOLD = 0.5;
	private static long AGENT_CYCLE_PERIOD = 500;
	private static String IDLE = "idle";
	private static String COOLING = "cooling";
	private static String HEATING = "heating";
	
	private double targetGoalTemperature;
	
	public ThermostatAgentStep1(RoomThingAPI roomThing, double targetGoalTemperature) {
		super(roomThing);
		this.targetGoalTemperature = targetGoalTemperature;
	}
	
	public void run() {
		log("Launched - working with room " + this.getThingId() + " - temperature to achieve: " + targetGoalTemperature);		
		log("Connecting to the room thing...");
		while (true) {
			try {
				double currentTemp = this.doGetTemperature();
				String state = this.doGetState();
				log("State: current temperature: " + currentTemp + " - state: " + state );
				if (currentTemp < (targetGoalTemperature - THRESOLD) && !state.equals(HEATING)) {
					log("too cold: start heating...");
					this.doStartHeating();
				} else if (currentTemp > (targetGoalTemperature + THRESOLD) && !state.equals(COOLING)) {
					log("too hot: start cooling...");
					this.doStartCooling();
				} else if (Math.abs(currentTemp - targetGoalTemperature) < THRESOLD) {
					log("achieved: stop working");
					if (!state.equals(IDLE)) {
						this.doStopWorking();
					}
					break;
				}
				waitTimeInMs(AGENT_CYCLE_PERIOD);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		log("GOAL ACHIEVED.");
	}
}
