package pc.smart_room.agent.thread_based;

import pc.smart_room.api.RoomThingAPI;

/**
 * 
 * Thread based agent whose task is to achieve and maintain a target temperature,
 * which can be specified and changed dynamically by a user through a GUI.
 * 
 * @author aricci
 *
 */
public class ThermostatAgentStep3 extends AbstractThreadBasedThermostatAgent {

	private static double THRESOLD = 0.5;
	private static String IDLE = "idle";
	private static String COOLING = "cooling";
	private static String HEATING = "heating";
	private static long AGENT_CYCLE_PERIOD = 500;
	private static long AGENT_CYCLE_LONG_PERIOD = 1000;
	private UserPrefPanel panel;

	
	public ThermostatAgentStep3(RoomThingAPI roomThing) {
		super(roomThing);
	}
	
	public void run() {
		log("launched - working with room thing " + this.getThingId());
		
		log("setting up GUI.");
		createUserGUI();
		
		log("waiting for user start.");
		waitForUserStart();
		
		while (true) {
			double currentTemp = this.doGetTemperature();
			String state = this.doGetState();
			double targetGoalTemperature = this.getUserTargetTemperature();
					
			log("Idle (State: current temperature: " + currentTemp + " - state: " + state +" - target temp: " + targetGoalTemperature + ")" );
			if (Math.abs(currentTemp - targetGoalTemperature) > THRESOLD) {
				boolean tempAchieved = false;
				while (!tempAchieved) {
					try {
						currentTemp = this.doGetTemperature();
						state = this.doGetState();
						targetGoalTemperature = this.getUserTargetTemperature();
	
						log("Acting (State: current temperature: " + currentTemp + " - state: " + state +" - target temp: " + targetGoalTemperature + ")" );
						if (currentTemp < (targetGoalTemperature - THRESOLD) && !state.equals(HEATING)) {
							log("too cold: start heating...");
							this.doStartHeating();
						} else if (currentTemp > (targetGoalTemperature + THRESOLD) && !state.equals(COOLING)) {
							log("too hot: start cooling...");
							this.doStartCooling();
						} else if (Math.abs(currentTemp - targetGoalTemperature) < THRESOLD) {
							log("achieved: stop working");
							if (!state.equals(IDLE)) {
								this.doStopWorking();
							}
							tempAchieved = true;
						}
						waitTimeInMs(AGENT_CYCLE_PERIOD);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
			waitTimeInMs(AGENT_CYCLE_LONG_PERIOD);
		}
	}
	
	// actions
	
	protected void createUserGUI() {
		panel = new 	UserPrefPanel();
		panel.display();
	}
	
	protected double getUserTargetTemperature() {
		return panel.getCurrentUserTemperature();
	}
	
	protected void waitForUserStart() {
		try {
			panel.waitForStart();
		} catch (Exception ex) {}
	}
}
