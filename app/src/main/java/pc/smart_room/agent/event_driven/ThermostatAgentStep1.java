package pc.smart_room.agent.event_driven;

import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import pc.smart_room.api.RoomThingAPI;

/**
 * 
 * Event-oriented simple agent whose task is to achieve a target temperature,
 * eventually reacting to temperature changes while achieving it.
 * 
 * @author aricci
 *
 */
public class ThermostatAgentStep1 extends AbstractEventDrivenThermostatAgent {

	private static double THRESOLD = 0.5;
	private static String IDLE = "idle";
	private static String COOLING = "cooling";
	private static String HEATING = "heating";
	private double targetGoalTemperature;

	private double currentTemperature;
	private String currentState;
	
	public ThermostatAgentStep1(RoomThingAPI roomThing, double targetGoalTemperature) {
		super(roomThing);
		this.targetGoalTemperature = targetGoalTemperature;
	}
	
	public void start() {
		log("Launched - working with room " + this.getThingId() + " - temperature to achieve: " + targetGoalTemperature);		
		
		/* pro-active part */
		
		Future<Double> currentTempFut = this.doGetTemperature();
		Future<String> currentStateFut = this.doGetState();
		
		CompositeFuture.all(currentTempFut, currentStateFut).onComplete(ar -> {
			if (ar.succeeded()) {
				currentTemperature = currentTempFut.result();
				currentState = currentStateFut.result();
				doCheck();
			}
		});
		
		/* reactive part */
		 
		this.startObservingThing(ev -> {
			log("new event observed: \n" + ev.encodePrettily());
			String evType = ev.getString("event");
			if (evType.equals("propertyStatusChanged")) {
				JsonObject data = ev.getJsonObject("data");
				Double newTemperature = data.getDouble("temperature");
				String newState = data.getString("state");
				if (newTemperature != null) {
					currentTemperature = newTemperature; 
				}
				if (newState != null) {
					currentState = newState; 
				}
				
				doCheck();
			}
		});
	}

	private void doCheck() {
		log("State: current temperature: " + currentTemperature + " - state: " + currentState );
		if (currentTemperature < (targetGoalTemperature - THRESOLD) && !currentState.equals(HEATING)) {
			log("too cold: start heating...");
			this.doStartHeating();
		} else if (currentTemperature > (targetGoalTemperature + THRESOLD) && !currentState.equals(COOLING)) {
			log("too hot: start cooling...");
			this.doStartCooling();
		} else if (Math.abs(currentTemperature - targetGoalTemperature) < THRESOLD) {
			log("achieved: stop working");
			if (!currentState.equals(IDLE)) {
				this.doStopWorking();
				try {
					this.stop();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}
}
