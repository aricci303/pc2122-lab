package pc.smart_room.agent.event_driven;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import pc.smart_room.api.RoomThingAPI;

/**
 * Abstract base class for event-driven thermostat agents.
 * 
 * @author aricci
 *
 */
public abstract class AbstractEventDrivenThermostatAgent extends AbstractVerticle {

	private RoomThingAPI roomThing;

	public AbstractEventDrivenThermostatAgent(RoomThingAPI roomThing) {
		this.roomThing = roomThing;
	}

	// actions

	protected String getThingId() {
		return roomThing.getId();
	}

	protected Future<String> doGetState() {
		return roomThing.getState();
	}

	protected Future<Double> doGetTemperature() {
		return roomThing.getTemperature();
	}

	protected Future<Void> doStartCooling() {
		return roomThing.startCooling();
	}

	protected Future<Void> doStartHeating() {
		return roomThing.startHeating();
	}

	protected Future<Void> doStopWorking() {
		return roomThing.stopWorking();
	}

	protected void startObservingThing(Handler<JsonObject> handler) {
		roomThing.subscribe(handler);
		/*
		 * WebSocketConnectOptions opt = new WebSocketConnectOptions();
		 * opt.addSubProtocol("webthing"); opt.setHost(roomThingURI); opt.setPort(port);
		 * opt.setURI("/"); log ("Connecting to the thing..."); wsClient.webSocket(opt,
		 * res -> { if (res.succeeded()) { log("Connected! Subscribing.");
		 * 
		 * WebSocket ws = res.result(); JsonObject msg = new JsonObject(); msg
		 * .put("messageType", "addEventSubscription") .put("data", new JsonObject());
		 * 
		 * ws.writeTextMessage(msg.encodePrettily());
		 * 
		 * ws.handler(handler); } });
		 */
	}

	protected void log(String msg) {
		System.out.println("[THERMOSTAT] " + msg);
	}

}
