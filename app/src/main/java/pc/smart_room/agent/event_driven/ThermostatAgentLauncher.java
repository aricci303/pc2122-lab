package pc.smart_room.agent.event_driven;

import io.vertx.core.Vertx;
import pc.smart_room.api.RoomThingProxy;

public class ThermostatAgentLauncher {

	
	public static void main(String[] args) {
		RoomThingProxy thing = new RoomThingProxy("my-room", "localhost", 8888);

		Vertx vertx = Vertx.vertx();

		// AbstractEventDrivenThermostatAgent agent = new ThermostatAgentStep1(thing, 17);
		// AbstractEventDrivenThermostatAgent agent = new ThermostatAgentStep2(thing, 17);
		AbstractEventDrivenThermostatAgent agent = new ThermostatAgentStep3(thing);
		
		vertx.deployVerticle(agent);
	}	

}
