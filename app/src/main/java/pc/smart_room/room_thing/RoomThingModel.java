package pc.smart_room.room_thing;

import java.awt.Dimension;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import pc.smart_room.api.RoomThingAPI;

/**
 * 
 * Behaviour of the Room Thing 
 * 
 * @author aricci
 *
 */
public class RoomThingModel implements RoomThingAPI {

	private Vertx vertx;

	private double temperature;
	private String state;
	
	private String thingId;
	private JsonObject td;

	/* simulator part */
	
	private static final int PERIOD = 100;
	
	private final ScheduledExecutorService scheduler =
		     Executors.newScheduledThreadPool(1);
	private Optional<ScheduledFuture<?>> handle = Optional.empty();
	private RoomSimulatorPanel panel;
	

	public RoomThingModel(String thingId) {
		log("Creating the room simulator.");
		this.thingId = thingId;

		
	    state = "idle";
	    temperature = 21;
	    
		log("Creating the panel.");
	    panel = new RoomSimulatorPanel();
	    SwingUtilities.invokeLater(() -> {
	    		panel.setVisible(true);
	    });
		
	}
	
	public void setup(Vertx vertx) {
		this.vertx = vertx;
	}

	@Override
	public String getId() {
		return thingId;
	}
	

	@Override
	public Future<Double> getTemperature() {
		Promise<Double> p = Promise.promise();
		synchronized (this) {
			p.complete(temperature);
		}
		return p.future();
	}

	@Override
	public Future<String> getState() {
		Promise<String> p = Promise.promise();
		synchronized (this) {
			p.complete(state);
		}
		return p.future();
	}

	@Override
	public Future<Void> startCooling() {
		Promise<Void> p = Promise.promise();
		if (handle.isPresent()) {
    		handle.get().cancel(true);
	    }
	    state = "cooling";

    	this.notifyNewPropertyStatus();
	    
		handle = Optional.of(scheduler.scheduleAtFixedRate(() -> {
			synchronized (this) {
				this.temperature -= 0.1;

		    	this.notifyNewPropertyStatus();
			    				
			}
			
			panel.updateTemp();
	     }, 0, PERIOD, TimeUnit.MILLISECONDS));
		p.complete();
		return p.future();
	}

	@Override
	public Future<Void> startHeating() {
		Promise<Void> p = Promise.promise();
		if (handle.isPresent()) {
    		handle.get().cancel(true);
	    }
	    state = "heating";
	    
    	this.notifyNewPropertyStatus();
	    	    
		handle = Optional.of(scheduler.scheduleAtFixedRate(() -> {
			synchronized (this) {
				this.temperature += 0.1;
				
		    	this.notifyNewPropertyStatus();
				
			}
			panel.updateTemp();
	     }, 0, PERIOD, TimeUnit.MILLISECONDS));
		p.complete();
		return p.future();		
	}

	@Override
	public Future<Void> stopWorking() {
		Promise<Void> p = Promise.promise();
		if (handle.isPresent()) {
			handle.get().cancel(true);
	    	handle = Optional.empty();
	    	state = "idle";
	    	this.notifyNewPropertyStatus();
	    }
		p.complete();
		return p.future();
	}
	
	private void notifyNewPropertyStatus() {
	    JsonObject ev = new JsonObject();
		ev.put("event", "propertyStatusChanged");
	    JsonObject data = new JsonObject();
		data.put("state", state);
		data.put("temperature", temperature);
		ev.put("data", data);			
		ev.put("timestamp", System.currentTimeMillis());
		this.generateEvent(ev);
	}

	private void generateEvent(JsonObject ev) {
		vertx.eventBus().publish("events", ev);	
	}
	
	public Future<Void> subscribe(Handler<JsonObject> h) {
		Promise<Void> p = Promise.promise();
		vertx.eventBus().consumer("events", ev -> {
			h.handle((JsonObject) ev.body());
		});	
		p.complete();
		return p.future();
	}
		
	protected void log(String msg) {
		System.out.println("[RoomThingModel]["+System.currentTimeMillis()+"] " + msg);
	}

	double readTemperatureForSimulatorPanel() {
		synchronized(this) {
			return this.temperature;
		}
	}

	void updateTemperatureFromSimulatorPanel(double temp) {
		synchronized(this) {
			this.temperature = temp;
	    	this.notifyNewPropertyStatus();
			
		}
	}
	
	
	class RoomSimulatorPanel extends JFrame {		
		
		private JTextField tempValue;
		private JSlider temp;
		
		public RoomSimulatorPanel(){
			setTitle("..:: Room Simulator Panel ::..");
			setSize(400,120);
			
			JPanel mainPanel = new JPanel();
			mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
			setContentPane(mainPanel);
			
			JPanel temperature = new JPanel();
			temperature.setLayout(new BoxLayout(temperature, BoxLayout.Y_AXIS));

			JPanel temperature1 = new JPanel();
			temperature1.setLayout(new BoxLayout(temperature1, BoxLayout.X_AXIS));
			
			tempValue = new JTextField(5);
			tempValue.setText("" + readTemperatureForSimulatorPanel());
			tempValue.setSize(100, 30);
			tempValue.setMinimumSize(tempValue.getSize());
			tempValue.setMaximumSize(tempValue.getSize());
			tempValue.setEditable(false);
			
			temperature1.add(new JLabel("Room physical temperature:"));
			temperature1.add(Box.createRigidArea(new Dimension(0,5)));
			temperature1.add(tempValue);
			
			temp = new JSlider(JSlider.HORIZONTAL, 5, 45, (int) readTemperatureForSimulatorPanel());
			temp.setSize(300, 60);
			temp.setMinimumSize(temp.getSize());
			temp.setMaximumSize(temp.getSize());
			temp.setMajorTickSpacing(10);
			temp.setMinorTickSpacing(1);
			temp.setPaintTicks(true);
			temp.setPaintLabels(true);
			
			temp.addChangeListener((ChangeEvent ev) -> {
				updateTemperatureFromSimulatorPanel( temp.getValue());
				tempValue.setText("" + readTemperatureForSimulatorPanel());
			});

			temperature.add(temperature1);
			temperature.add(temp);
			
			mainPanel.add(temperature);
		}
		
		public void updateTemp() {
			SwingUtilities.invokeLater(() -> {
				tempValue.setText(String.format("%.2f", readTemperatureForSimulatorPanel()));
			});
		}
	}
	
}
