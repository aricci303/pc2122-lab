package pc.smart_room.room_thing;

import io.vertx.core.Vertx;

/**
 * Launching the Room Thing service.
 * 
 * @author aricci
 *
 */
public class RoomThingServiceLauncher {

	static final int ROOM_THING_PORT = 8888;
	
	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();

		RoomThingModel model = new RoomThingModel("my-room");
		model.setup(vertx);
		
		vertx.deployVerticle(new RoomThingService(model, ROOM_THING_PORT));
	}

}
