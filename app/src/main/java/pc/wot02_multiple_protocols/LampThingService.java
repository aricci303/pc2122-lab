package pc.wot02_multiple_protocols;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Promise;

public class LampThingService extends AbstractVerticle {

	private LampThingAPI model;
	private List<LampThingAbstractAdapter> adapters;
	
	public LampThingService(LampThingAPI model) {
		this.model = model;
		adapters = new LinkedList<LampThingAbstractAdapter>();
	}
	
	@Override
	public void start(Promise<Void> startPromise) throws Exception {
		installAdapters(startPromise);
	}	
	
	/**
	 * Installing all available adapters.
	 * 
	 * Typically driven by using some config file.
	 *  	 
	 */
	protected void installAdapters(Promise<Void> promise) {		
		ArrayList<Future> allFutures = new ArrayList<Future>();
		
		try {
			/*
			 * Installing HTTP adapter.
			 */
			LampThingAbstractAdapter httpAdapter = new LampThingHTTPAdapter(model, 8080, this.getVertx());
			Promise<Void> p = Promise.promise();
			httpAdapter.setupAdapter(p);
			Future<Void> fut = p.future();
			allFutures.add(fut);
			fut.onSuccess(res -> {
				log("HTTP adapter installed.");
				adapters.add(httpAdapter);
			}).onFailure(f -> {
				log("HTTP adapter not installed.");
			});
		} catch (Exception ex) {
			log("HTTP adapter installation failed.");
		}
		
		try {
			/*
			 * Installing MQTT adapter.
			 */
			LampThingAbstractAdapter mqttAdapter = new LampThingMQTTAdapter(model, "tcp://localhost:1883", this.getVertx());
			Promise<Void> p = Promise.promise();
			mqttAdapter.setupAdapter(p);
			adapters.add(mqttAdapter);
			Future<Void> fut = p.future();
			allFutures.add(fut);
			fut.onSuccess(res -> {
				log("MQTT adapter installed.");
				adapters.add(mqttAdapter);
			}).onFailure(f -> {
				log("MQTT adapter not installed.");
			});
		} catch (Exception ex) {
			log("MQTT adapter installation failed.");
		}
		
		CompositeFuture.all(allFutures).onComplete(res -> {
			log("Adapters installed.");
			promise.complete();
		});
	}

	protected void log(String msg) {
		System.out.println("[LampThingService] " + msg);
	}
}
