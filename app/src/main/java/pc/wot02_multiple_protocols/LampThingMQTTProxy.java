package pc.wot02_multiple_protocols;

import java.net.URI;
import java.util.HashMap;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;

/**
 * Proxy to interact with a LampThing using MQTT protocol
 * 
 * @author aricci
 *
 */
public class LampThingMQTTProxy implements LampThingAPI {

	private Vertx vertx;

    private MqttClient client;	

    private final int qos = 1;
	private URI uri;
	private String thingId;
	private String consumerId;
	
	private String thingRequestsTopic;
	private String consumerRepliesTopic;
	private String thingEventsTopic;
	
	private long reqId;
	private HashMap<Long,Promise> pendingReqs;

		
	public LampThingMQTTProxy(String consumerId, String thingId, String uri) throws Exception {
		this.consumerId = consumerId;
		this.thingId = thingId;
		reqId = 0;
		this.uri = new URI(uri);
		pendingReqs = new HashMap<Long,Promise>();
	}
	
	@Override
	public String getId() {
		return thingId;
	}
	
	public Future<Void> setup(Vertx vertx) {
		this.vertx = vertx;
		Promise<Void> promise = Promise.promise();
		
        String host = String.format("tcp://%s:%d", uri.getHost(), uri.getPort());
        try {
	        MqttConnectOptions conOpt = new MqttConnectOptions();
	        conOpt.setCleanSession(true);
	
	        this.thingRequestsTopic = "things/" + thingId + "/requests";
	        this.thingEventsTopic = "things/" + thingId + "/events";
	        this.consumerRepliesTopic = "consumers/" + consumerId + "/replies";
	        		
	        this.client = new MqttClient(host, consumerId, new MemoryPersistence());
	        this.client.setCallback(new AdapterMqttCallback(this));
	        this.client.connect(conOpt);
	        
	        /* callback to get MQTT messages about replies - not on the event loop */
	        this.client.subscribe(consumerRepliesTopic, qos);
	        
	        /* handler to process MQTT msgs on the event-loop */ 
	        vertx.eventBus().consumer("replies", this::handleReplies);   
	        
	        promise.complete();
        } catch (Exception ex) {
        	ex.printStackTrace();
        	promise.fail(ex.getMessage());
        }
        return promise.future();
	        
	}
	
	public Future<Boolean> isOn() {
		Promise<Boolean> promise = Promise.promise();
		JsonObject req = new JsonObject();
		reqId++;
		req.put("request", "readStatus");
		req.put("reqId", reqId);
		req.put("replyTopic", consumerRepliesTopic);
		this.sendMsg(this.thingRequestsTopic, req);
		pendingReqs.put(reqId, promise);
		return promise.future();
	}
	
	public Future<Void> switchOn() {
		Promise<Void> promise = Promise.promise();
		JsonObject req = new JsonObject();
		reqId++;
		req.put("request", "switchOn");
		req.put("reqId", reqId);
		req.put("replyTopic", consumerRepliesTopic);
		this.sendMsg(this.thingRequestsTopic, req);
		pendingReqs.put(reqId, promise);
		return promise.future();
	}
	
	public Future<Void> switchOff() {
		Promise<Void> promise = Promise.promise();
		JsonObject req = new JsonObject();
		reqId++;
		req.put("request", "switchOff");
		req.put("reqId", reqId);
		req.put("replyTopic", consumerRepliesTopic);
		this.sendMsg(this.thingRequestsTopic, req);
		pendingReqs.put(reqId, promise);
		return promise.future();
	}

	public Future<Void> subscribe(Handler<JsonObject> handler) {
        Future<Void> fut = vertx.executeBlocking(p -> {
    		try {
            	this.client.subscribe(thingEventsTopic, qos);
            	this.vertx.eventBus().consumer("events", msg -> {
            		handler.handle((JsonObject) msg.body());
            	});
            	p.complete();
            } catch (Exception ex) {
            	p.fail("subscription failed");
            }
        });
        return fut;
	}
		

	private void sendMsg(String topic, JsonObject msg) {
		vertx.executeBlocking(promise -> {
			try {
				String payload = msg.encode();
		        MqttMessage message = new MqttMessage(payload.getBytes());
		        message.setQos(qos);
		        this.client.publish(topic, message); // Blocking publish
		        promise.complete();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		});    		
	}
		
	void notifyReply(JsonObject reply) {
    	vertx.eventBus().publish("replies", reply);    	
	}

	void notifyEvent(JsonObject ev) {
    	vertx.eventBus().publish("events", ev);    	
	}

	/**
	 * Handler to process replies arrived as MQTT messages
	 * 
	 * @param msg
	 */
	private void handleReplies(Message<JsonObject> msg) {
    	JsonObject reply = msg.body();
    	long reqId = reply.getLong("reqId");    	
    	Promise p = pendingReqs.get(reqId);    	
    	String result = reply.getString("reply");
    	if (result.equals("ok")) {
	    	String reqType = reply.getString("request");
	    	if (reqType.equals("readStatus")) {
	    		String status = reply.getString("status");
	    		((Promise<Boolean>) p).complete(status.equals("on"));
	    	} else {
	    		p.complete();
	    	}
    	} else {
    		p.fail(result);
    	}
	}	
	
	protected void log(String msg) {
		System.out.println("[LampThingMQTTProxy]["+System.currentTimeMillis()+"] " + msg);
	}

	
	/**
	 * MQTT Adapter 
	 * 
	 * @author aricci
	 *
	 */
	class AdapterMqttCallback implements MqttCallback {
		private LampThingMQTTProxy adapter;
		
		public AdapterMqttCallback(LampThingMQTTProxy adapter) {
			this.adapter = adapter;
		}
				
	    public void messageArrived(String topic, MqttMessage message) throws MqttException {
	    	if (topic.equals(consumerRepliesTopic)) {
	    		adapter.notifyReply(new JsonObject(new String(message.getPayload())));
	    	} else if (topic.equals(thingEventsTopic)) {
	    		adapter.notifyEvent(new JsonObject(new String(message.getPayload())));
	    	}
	    }	

	    public void connectionLost(Throwable cause) {}
	    public void deliveryComplete(IMqttDeliveryToken token) {}
	}
	
}
