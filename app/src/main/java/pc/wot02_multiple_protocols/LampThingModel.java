package pc.wot02_multiple_protocols;

import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;

/**
 * 
 * Behaviour of the Lamp Thing 
 * 
 * - independent from specific binding protocols
 * 
 * @author aricci
 *
 */
public class LampThingModel implements LampThingAPI {

	private boolean isOn;
	private Vertx vertx;
	private String thingId;
	
	public LampThingModel(String thingId) {
		isOn = false;
		this.thingId = thingId;
		log("LampThing model created.");
	}
	
	public void setup(Vertx vertx) {
		this.vertx = vertx;
	}

	@Override
	public String getId() {
		return thingId;
	}
	
	public Future<Void> switchOn() {
		Promise<Void> p = Promise.promise();
		isOn = true;
		log("Switched on.");
		JsonObject ev = new JsonObject();
		ev.put("event", "switchedOn");
		ev.put("timestamp", System.currentTimeMillis());
		this.generateEvent(ev);
		p.complete();
		return p.future();
	}

	public Future<Void> switchOff() {
		Promise<Void> p = Promise.promise();
		isOn = false;
		log("Switched off.");
		JsonObject ev = new JsonObject();
		ev.put("event", "switchedOff");
		ev.put("timestamp", System.currentTimeMillis());
		this.generateEvent(ev);
		p.complete();
		return p.future();
	}

	public Future<Boolean> isOn() {
		Promise<Boolean> p = Promise.promise();
		p.complete(isOn);
		return p.future();
	}

	private void generateEvent(JsonObject ev) {
		vertx.eventBus().publish("events", ev);	
	}
	
	public Future<Void> subscribe(Handler<JsonObject> h) {
		Promise<Void> p = Promise.promise();
		vertx.eventBus().consumer("events", ev -> {
			h.handle((JsonObject) ev.body());
		});	
		p.complete();
		return p.future();
	}

	protected void log(String msg) {
		System.out.println("[LampThingModel]["+System.currentTimeMillis()+"] " + msg);
	}

	
}
