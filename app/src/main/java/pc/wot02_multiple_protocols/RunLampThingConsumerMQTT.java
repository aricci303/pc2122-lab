package pc.wot02_multiple_protocols;

import io.vertx.core.Future;
import io.vertx.core.Vertx;

public class RunLampThingConsumerMQTT {

	public static void main(String[] args) throws Exception {
		Vertx vertx = Vertx.vertx();
		
		LampThingMQTTProxy thing = new LampThingMQTTProxy("my-consumer","my-lamp", "tcp://localhost:1883");
		Future<Void> fut = thing.setup(vertx);
		
		fut.onSuccess(h -> {
			vertx.deployVerticle(new LampThingConsumerAgent(thing));
		});
	}

}
