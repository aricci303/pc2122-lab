package pc.wot02_multiple_protocols;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;

public class LampThingConsumerAgent extends AbstractVerticle {

	
	private LampThingAPI thing;
	private int nEventsReceived;
	
	public LampThingConsumerAgent(LampThingAPI thing) {
		this.thing = thing; 
		nEventsReceived = 0;
	}
	
	/**
	 * Main agent body.
	 */
	public void start(Promise<Void> startPromise) throws Exception {
		log("Lamp consumer agent started.");		
		
		log("Getting the status...");		
		Future<Boolean> isOnRes = thing.isOn();

		Future<Void> switchOnRes = isOnRes.compose(res -> {
			log("Status: \n" + (res.booleanValue() ? "on" : "off"));			
			log("Switching on");
			return thing.switchOn();
		}).onFailure(err -> {
			log("Failure " + err);
		});
		
		Future<Void> subscribeRes = switchOnRes.compose(res2 -> {
			log("Action done. "); 				
			log("Subscribing...");
			return thing.subscribe(this::onNewEvent);
		});

		subscribeRes.onComplete(res3 -> {
			log("Subscribed!");
		});		
	}
	
	
	/**
	 * Handler to process observed events  
	 */
	protected void onNewEvent(JsonObject ev) {
		nEventsReceived++;
		log("New event: \n " + ev.toString() + "\nNum events received: " + nEventsReceived);
	}
	
	protected void log(String msg) {
		System.out.println("[LampThingConsumerAgent]["+System.currentTimeMillis()+"] " + msg);
	}
	
}
