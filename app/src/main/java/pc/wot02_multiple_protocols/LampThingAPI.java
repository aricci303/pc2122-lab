package pc.wot02_multiple_protocols;

import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;

/**
 * LampThing API 
 * 
 * - both used on consumer (client) and model (server) side
 * - independent from the specific protocols
 * 
 * @author aricci
 *
 */
public interface LampThingAPI {
	
	String getId();
	
	Future<Boolean> isOn();
	
	Future<Void> switchOn();

	Future<Void> switchOff();

	Future<Void> subscribe(Handler<JsonObject> handler);
}
