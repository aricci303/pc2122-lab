package pc.wot02_multiple_protocols;

import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.WebSocket;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;

/**
 * Proxy to interact with a LampThing using HTTP protocol
 * 
 * @author aricci
 *
 */
public class LampThingHTTPProxy implements LampThingAPI {

	private Vertx vertx;
	private WebClient client;

	private String thingId;
	private int thingPort;
	private String thingHost;

	private static final String PROPERTY_STATUS = "/api/properties/status";
	private static final String ACTION_SWITCHON = "/api/actions/switchOn";
	private static final String ACTION_SWITCHOFF = "/api/actions/switchOff";
	private static final String EVENTS = "/api/events";
		
	public LampThingHTTPProxy(String thingId, String thingHost, int thingPort){
		this.thingId = thingId;
		this.thingPort = thingPort;
		this.thingHost = thingHost;
	}

	public Future<Void> setup(Vertx vertx) {
		this.vertx = vertx;
		Promise<Void> promise = Promise.promise();
		vertx.executeBlocking(p -> {
			client = WebClient.create(vertx);
			promise.complete();
		});
		return promise.future();
	}
	
	public Future<Boolean> isOn() {
		Promise<Boolean> promise = Promise.promise();
		client
			.get(this.thingPort, thingHost, PROPERTY_STATUS)
			.send()
			.onSuccess(response -> {
				JsonObject reply = response.bodyAsJsonObject();
				String status = reply.getString("status");
				promise.complete(status.equals("on"));
			})
			.onFailure(err -> {
				promise.fail("Something went wrong " + err.getMessage());
			});
		return promise.future();
	}
	
	public Future<Void> switchOn() {
		Promise<Void> promise = Promise.promise();
		client
			.post(this.thingPort, thingHost, ACTION_SWITCHON)
			.send()
			.onSuccess(response -> {
				promise.complete(null);
			})
			.onFailure(err -> {
				promise.fail("Something went wrong " + err.getMessage());
			});
		return promise.future();
	}
	
	public Future<Void> switchOff() {
		Promise<Void> promise = Promise.promise();
		client
			.post(this.thingPort, thingHost, ACTION_SWITCHOFF)
			.send()
			.onSuccess(response -> {
				promise.complete(null);
			})
			.onFailure(err -> {
				promise.fail("Something went wrong " + err.getMessage());
			});
		return promise.future();
	}

	public Future<Void> subscribe(Handler<JsonObject> handler) {
		Promise<Void> promise = Promise.promise();
		HttpClient cli = vertx.createHttpClient();
		cli.webSocket(this.thingPort, thingHost, EVENTS, res -> {
			if (res.succeeded()) {
				log("Connected!");
				WebSocket ws = res.result();
				ws.handler(buf -> {
					handler.handle(buf.toJsonObject());
				});
				promise.complete();
			}
		});
		return promise.future();			
	}
	
	protected void log(String msg) {
		System.out.println("[LampThingHTTPProxy]["+System.currentTimeMillis()+"] " + msg);
	}

	@Override
	public String getId() {
		return thingId;
	}
	
}
