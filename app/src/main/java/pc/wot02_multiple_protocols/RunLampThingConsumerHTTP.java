package pc.wot02_multiple_protocols;

import io.vertx.core.Future;
import io.vertx.core.Vertx;

public class RunLampThingConsumerHTTP {

	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();
	
		LampThingHTTPProxy thing = new LampThingHTTPProxy("my-lamp", "localhost", 8080);
		Future<Void> fut = thing.setup(vertx);
		fut.onSuccess(h -> {
			vertx.deployVerticle(new LampThingConsumerAgent(thing));
		});
	}

}
