package pc.wot02_multiple_protocols;

import io.vertx.core.Vertx;

public class RunLampThingService {

	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();

		LampThingModel model = new LampThingModel("my-lamp");
		model.setup(vertx);
		
		vertx.deployVerticle(new LampThingService(model));
	}

}
