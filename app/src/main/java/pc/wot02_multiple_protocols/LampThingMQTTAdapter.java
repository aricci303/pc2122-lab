package pc.wot02_multiple_protocols;

import java.net.URI;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;

public class LampThingMQTTAdapter extends LampThingAbstractAdapter {

    private MqttClient client;	
    private final int qos = 1;
	private URI uri;
	private String thingRequestsTopic;
	private String thingEventsTopic;
	
	public LampThingMQTTAdapter(LampThingAPI model, String uri, Vertx vertx) throws Exception {
		super(model, vertx);
		this.uri = new URI(uri);
	}
	
	protected void setupAdapter(Promise<Void> promise) {
        String host = String.format("tcp://%s:%d", uri.getHost(), uri.getPort());
        try {
	        MqttConnectOptions conOpt = new MqttConnectOptions();
	        conOpt.setCleanSession(true);

	        String thingId = this.getModel().getId();
	        this.thingRequestsTopic = "things/" + thingId +"/requests";
	        this.thingEventsTopic = "things/"+ thingId  +"/events";

	        this.client = new MqttClient(host, thingId, new MemoryPersistence());
	        this.client.setCallback(new AdapterMqttCallback(this));
	        this.client.connect(conOpt);
	        this.client.subscribe(thingRequestsTopic, qos);
	        
	        this.getVertx().eventBus().consumer("requests", this::handleRequests);

			this.getModel().subscribe(ev -> {
				sendMsg(thingEventsTopic, ev);
			});
			log("MQTT adapter working with broker at: " + host);
	        promise.complete();
        } catch (Exception ex) {
        	ex.printStackTrace();
        	promise.fail(ex.getMessage());
        }
        
	}

	private void handleRequests(Message<String> msg) {
    	JsonObject request = new JsonObject(msg.body());
    	String reqType = request.getString("request");
    	String consumerReplyTopic = request.getString("replyTopic");
    	long reqId = request.getLong("reqId");
    	
		JsonObject reply = new JsonObject();
		reply.put("reqId", reqId);
		reply.put("request", reqType);
		
		if (reqType.equals("readStatus")) {    	
    		reply.put("reply", "ok");
    		Future<Boolean> status = this.getModel().isOn();
    		status.onSuccess(res -> {
    			reply.put("status", res.booleanValue() ? "on" : "off");
    			sendMsg(consumerReplyTopic, reply);		
    		});
    	} else if (reqType.equals("switchOn")) {
    		Future<Void> fut = this.getModel().switchOn();
    		fut.onSuccess(ret -> {
	    		reply.put("reply", "ok");
    			sendMsg(consumerReplyTopic, reply);		
    		});
    	} else if (reqType.equals("switchOff")) {
    		Future<Void> fut = this.getModel().switchOff();
    		fut.onSuccess(ret -> {
	    		reply.put("reply", "ok");
    			sendMsg(consumerReplyTopic, reply);		
    		});
    	} else {
    		reply.put("reply", "error");
			sendMsg(consumerReplyTopic, reply);		
    	}
	}
		
	private void sendMsg(String topic, JsonObject msg) {
		this.getVertx().executeBlocking(promise -> {
			try {
				String payload = msg.encode();
		        MqttMessage message = new MqttMessage(payload.getBytes());
		        message.setQos(qos);
		        this.client.publish(topic, message); // Blocking publish
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		});    		
	}

	protected void log(String msg) {
		System.out.println("[LampThingMQTTAdapter]["+System.currentTimeMillis()+"] " + msg);
	}
	
	void notifyRequest(String req) {
    	this.getVertx().eventBus().publish("requests", req);    	
	}
	
	class AdapterMqttCallback implements MqttCallback {
		private LampThingMQTTAdapter adapter;
		
		public AdapterMqttCallback(LampThingMQTTAdapter adapter) {
			this.adapter = adapter;
		}
				
	    public void messageArrived(String topic, MqttMessage message) throws MqttException {
	    	adapter.notifyRequest(new String(message.getPayload()));
	    }	

	    public void connectionLost(Throwable cause) {}

	    public void deliveryComplete(IMqttDeliveryToken token) {}
	}
}
