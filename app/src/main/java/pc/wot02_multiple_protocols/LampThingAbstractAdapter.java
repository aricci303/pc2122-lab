package pc.wot02_multiple_protocols;

import io.vertx.core.Promise;
import io.vertx.core.Vertx;

public abstract class LampThingAbstractAdapter  {

	private Vertx vertx;
	
	private LampThingAPI model;
	
	protected LampThingAbstractAdapter(LampThingAPI model, Vertx vertx) {
		this.model = model;
		this.vertx = vertx;
	}
	
	protected Vertx getVertx() {
		return vertx;
	}

	protected LampThingAPI getModel() {
		return model;
	}
	
	abstract protected void setupAdapter(Promise<Void> startPromise);
	
}
